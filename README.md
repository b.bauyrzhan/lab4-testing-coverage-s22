# Lab4 -- Testing coverage 


## Introduction

Ok, so we've done our previous lab on the testing, but we were just doing one test per function, is that enough? I don't want to be the one to be boring here, but it's definitely not the case. Today we are going to practice with different approaches to test your project, depending on the picked coverage method.   ***Let's roll!***

## Code coverage

There exists a lot of different approaches for the code coverage, such as:
- Line coverage(or statement coverage, which is the weakest one, try not to use this one in prod)
- Branch coverage
- Basis path coverage
- Full path coverage (That's insane, use it only in the most important cases)
- Multiple condition coverage
- MC/DC coverage
You have already discussed this coverage on the lecture, so we won't stop here, describing each one, less talk - more code :)


## Lab

I was astonishingly lazy today, so we will just use the project we've worked on the previous lab today:) , you may copy your tests if you want to.
1. Create your fork of the `
Lab4 - Testing coverage
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab4-testing-coverage-and-integration-testing)
2. We've worked on few functions in `src/main/java/com/hw/db/controllers/forumController.java`, on the previous lab, now we are going to cover them with tests properly. We are going to use the branch coverage for our first function, and MC/DC for the second one, also we are going to write one integration test to check the proper work of DAO with controllers:
- Function for branch test:
```java
public ResponseEntity create(@RequestBody Forum forum) {
        User creator=new User();
        try {
            creator = UserDAO.Search(forum.getUser());
        } catch(DataAccessException Except)
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Владелец форума не найден."));
        }


        forum.setUser(creator.getNickname());
        try {
            ForumDAO.CreateForum(forum);
        } catch (DuplicateKeyException Except) {
            Forum exForum = Info(forum.getSlug());
            return ResponseEntity.status(HttpStatus.CONFLICT).body(exForum);
        } catch (DataAccessException Except)
        {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Что-то на сервере."));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(forum);

    }
```
As you can see we have a lot of try/catch blocks here, try/catch is basically an if statement of its own, but instead of boolean it checks for exceptions, so we may test try/catch blocks the same way we are handling exceptions.
Now let's write tests to have a full branch coverage:
```java
 class forumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("Not authorithed user fails to create forum")
    void noLoginWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(null);
            forumController controller = new forumController();
            controller.create(toCreate);
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Владелец форума не найден.")), controller.create(toCreate), "Result when unauthorised user tried to create a forum");
        }
    }

    @Test
    @DisplayName("User fails to create forum because it already exists")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DuplicateKeyException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(toCreate), controller.create(toCreate), "Result for conflict while forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
    
    @Test
    @DisplayName("User fails to create forum because server cannot access DB")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Что-то на сервере.")), controller.create(toCreate), "Result for DB error while server creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
}

```
But that was pretty boring, let's have interesting example where we can have `if's` that can intersect with each other, like this:
- Function for MC/DC:
```java
public static List<Thread> ThreadList(String slug, Number limit, String since, Boolean desc) {
        String SQL="SELECT * FROM threads WHERE forum = (?)::CITEXT";
        List<Object> conditions=new ArrayList<>();
        conditions.add(slug);
        if(since!=null)
        {
            if(desc!=null && desc) {
                SQL += " AND created<=(?)::TIMESTAMP WITH TIME ZONE";
            }  else
            {
                SQL += " AND created>=(?)::TIMESTAMP WITH TIME ZONE";

            }
            conditions.add(since);
        }
        SQL+=" ORDER BY created";
        if(desc!=null && desc)
        {
            SQL+=" desc";
        }
        if(limit!=null)
        {
            SQL+=" LIMIT ?";
            conditions.add(limit);
        }
        SQL+=";";
        return jdbc.query(SQL ,conditions.toArray() ,THREAD_MAPPER );
    }
```
Ok, base on this function, we have 4 different conditions totally, and we have 3 variables that are used in if expressions, let's create a table that will help us with our test-cases:

| Test case  | since | desc | limit | Expected result(SQL) |
|------------|-------|------|-------|----------------------|
|     1      | null  | false | null  | `SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;` |
|     2      | "12.02.2019"  | false | null  | `SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP ORDER BY created WITH TIME ZONE;` |
|     3      | "12.02.2019"  | true | null  | `SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP ORDER BY created WITH TIME ZONE desc;` |
|     4      | "12.02.2019"  | true | 10  | `SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP ORDER BY created WITH TIME ZONE desc LIMIT ?;` |
|     5      | "12.02.2019"  | false | 10  | `SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP ORDER BY created WITH TIME ZONE LIMIT ?;` |
|     6      | null  | true | 10  | `SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;` |

The problem here(it's actually not a problem) that we will need to test all the testcases, because *all the expected results are the same*.
Let's write tests to test all the ifs:
```java
    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }
// Other tests will be written on the lesson
```
3. Alright, and now let's add our tests to CI, it is pretty easy to do, you will need to write lines like this:
```yml
test_mcdc:
  stage: test
  image: maven
  script:
    - chmod +x mvnw
    - ./mvnw test -Dtest=forumDAOTests

test_branch:
  stage: test
  image: maven
  script:
    - chmod +x mvnw
    - ./mvnw test -Dtest=forumControllerTests
```

Here we go green again :)


## Homework

As a homework you will need to develop full branch coverage for function `UserList`(in `ForumDAO.java`), full MC/DC coverage for function `Change`(in `UserDAO.java`), full Basis path coverage for function `setPost`(in `PostDAO.java`) and full statement coverage for function `treeSort` (in `ThreadDAO.java`). After that you should add all your tests to the pipeline as a different pipes. All those pipes should run concurrently(as shown on the lab). **P.S. Develop tests for different classes in different files.**
**Lab is counted as done, if pipelines are passing. and tests are developed**

